export interface Auto {
  brand: string;
  model: string;
  description: string;
  id: number;
}
