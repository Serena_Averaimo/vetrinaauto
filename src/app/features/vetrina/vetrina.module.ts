import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { VetrinaComponent } from './vetrina.component';
import { RouterModule } from '@angular/router';



@NgModule({
  declarations: [VetrinaComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      { path: '', component: VetrinaComponent }
    ])
  ],
  exports: [
    VetrinaComponent
  ]
})
export class VetrinaModule { }
