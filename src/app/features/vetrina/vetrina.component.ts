import { AutoService } from './../../core/services/auto.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-vetrina',
  templateUrl: './vetrina.component.html',
  styleUrls: ['./vetrina.component.css']
})
export class VetrinaComponent implements OnInit {
  auto$ = this.autoService.getAuto();

  constructor(private readonly autoService: AutoService) { }

  ngOnInit(): void {
  }

}
