import { AutoService } from './../../core/services/auto.service';
import { HttpClient } from '@angular/common/http';
import { Auto } from './../../model/auto.model';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';

const URL = 'http://localhost:3000';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @Output() readonly save: EventEmitter<Auto> = new EventEmitter();


  brands = [
    {name: 'Audi', brandid: 0},
    {name: 'BMW', brandid: 1},
    {name: 'Mercedes', brandid: 2},
    {name: 'Tesla', brandid: 3}
  ];

  readonly form = this.fb.group({
    brand: [null, Validators.compose(
      [Validators.required, Validators.minLength(3)]
    )],
    model: [null, Validators.compose(
      [Validators.required, Validators.minLength(2)]
    )],
    description: [null, Validators.compose(
      [Validators.required, Validators.minLength(3)]
    )]
  });
  auto: Auto[];

  constructor(private readonly fb: FormBuilder,
              private readonly http: HttpClient) {

              }

  ngOnInit(): void {

  }
  add(){
    if(this.form.valid){
      console.log(JSON.stringify(this.form.value.brand));
      // this.http.post(`${URL}/auto`, this.form.value)
      // .subscribe((response)=>{
      //   console.log('repsonse ',response);
      // })
    }
  }


  // add(form: NgForm) {
  //   this.http.post<Auto>(`${URL}/auto`, form.value)
  //     .subscribe((result: Auto) => {
  //       this.auto.push(result);
  //     });
  //     console.log(this.auto);
  // }


}
