import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: 'form', loadChildren: () => import('../app/features/form/form.module').then(m => m.FormModule) },
  { path: 'vetrina', loadChildren: () => import('../app/features/vetrina/vetrina.module').then(m => m.VetrinaModule) },
  { path: '', loadChildren: './features/form/form.module#FormModule' },

];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
