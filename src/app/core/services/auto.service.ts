import { Auto } from './../../model/auto.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


const URL = 'http://localhost:3000';

@Injectable({
  providedIn: 'root'
})
export class AutoService {

  constructor(private readonly http: HttpClient) { }

  getAuto() {
    return this.http.get<Auto[]>(`${URL}/auto`)

  }


}
